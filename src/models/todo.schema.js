const mongoose = require('mongoose');

const { Schema } = mongoose;

mongoose.set('useCreateIndex', true);

const TodoSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
},
{
  timestamps: true, // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('Todo', TodoSchema);

const express = require('express');

const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

router.get('/metrics', (req, res, next) => {
  res.send('app_status 1');
});

module.exports = router;

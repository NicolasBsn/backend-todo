const express = require('express');

const router = express.Router();

const todoController = require('../controllers/todos.controller');

router.get('/', todoController.getTodos);
router.delete('/:id', todoController.deleteTodo);
router.put('/:id', todoController.updateTodo);
router.post('/', todoController.addTodo);


module.exports = router;

const Todo = require('../models/todo.schema');

class todosController {
  async addTodo(req, res) {
    try {
      const newTodo = new Todo({ title: req.body.title, description: req.body.description });
      await newTodo.save();
      res.json({ created: 'done' });
    } catch (err) {
      res.json({ error: err.message });
    }
  }

  async deleteTodo(req, res) {
    await Todo.findByIdAndRemove(req.params.id, (err) => {
      if (err) res.json({ error: err.message });
      res.json({ success: true });
    });
  }

  async updateTodo(req, res) {
    await Todo.findByIdAndUpdate(req.params.id, req.body, (err) => {
      if (err) res.json({ error: err.message });
      res.json({ success: true });
    });
  }

  async getTodos(req, res) {
    try {
      const todos = await Todo.find({}).exec();
      res.json({ data: todos });
    } catch (err) {
      res.json({ error: err.message });
    }
  }
}

module.exports = new todosController();

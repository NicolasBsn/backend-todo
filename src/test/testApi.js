
const chai = require('chai');
const chaiHttp = require('chai-http');
const Todo = require('../models/todo.schema');
const server = require('../app');

const should = chai.should();

chai.use(chaiHttp);

describe('Todos', () => {
  beforeEach((done) => {
    Todo.deleteMany({}, (err) => {
      done();
    });
  });

  describe('/GET Todos', () => {
    it('it should GET all the todos', (done) => {
      chai.request(server)
        .get('/todos')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe('/POST todo', () => {
    it('it should POST a todo', (done) => {
      const todo = {
        title: 'Important thing',
        description: 'Hey',
      };
      chai.request(server)
        .post('/todos')
        .send(todo)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('created');
          done();
        });
    });
  });

  describe('not /POST todo', () => {
    it('it should not POST a todo, a field is missing', (done) => {
      const todo = {
        title: 'Important thing',
      };
      chai.request(server)
        .post('/todos')
        .send(todo)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          done();
        });
    });
  });

  describe('not /POST todo', () => {
    it('it should not POST a todo, a field contains empty string', (done) => {
      const todo = {
        title: 'Important thing',
        description: '',
      };
      chai.request(server)
        .post('/todos')
        .send(todo)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          done();
        });
    });
  });

  describe('/delete/+:id todo', () => {
    it('it should DELETE a TODO with the corresponding id', (done) => {
      const todo = new Todo({ title: 'TODO', description: 'Eat' });
      todo.save((err, todo) => {
        chai.request(server)
          .delete(`/todos/${todo._id}`)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
          });
      });
    });
  });

  describe('/PUT todo', () => {
    it('it should PUT a todo', (done) => {
      const todo = new Todo({ title: 'Important thing', description: "I'm chan" });
      todo.save((err, todo) => {
        chai.request(server)
          .put(`/todos/${todo._id}`)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
          });
      });
    });
  });
});

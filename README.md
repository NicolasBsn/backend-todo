# Stupid Todo

This project, created during the DevOps course, uses ExpressJs, mocha/chai for testing and docker for packaging the app and the database.

It is a simple web-server for creating, deleting TODOs to illustrate CI/CD.

## Getting Started

Follow this Readme in order to run this webserver on your machine.

### Installing
* Start the tests with ```npm test```
* Start the webapp with ```npm run start```

### Routes
* GET the todos : ```localhost:3000/todos```
* POST a todos : ```localhost:3000/todos```
* DELETE a todo : ```localhost:3000/todos/:id```
* PUT a todo : ```localhost:3000/todos/:id```



### Contributor

Nicolas Boussenina, ECE Paris

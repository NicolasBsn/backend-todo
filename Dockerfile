FROM node:latest as test
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
EXPOSE 3000
CMD ./node_modules/mocha/bin/mocha ./src/test/*.js

FROM test as prod
CMD node ./src/app.js

